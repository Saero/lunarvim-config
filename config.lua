-- Read the docs: https://www.lunarvim.org/docs/configuration
-- Video Tutorials: https://www.youtube.com/watch?v=sFA9kX-Ud_c&list=PLhoH5vyxr6QqGu0i7tt_XoVK9v-KvZ3m6
-- Forum: https://www.reddit.com/r/lunarvim/
-- Discord: https://discord.com/invite/Xb9B4Ny
lvim.keys.normal_mode["<C-s>"] = ":w<cr>" -- faster than :w , the <cr> is probably carriage return, basically "Enter", override doesn't work w/o it
lvim.keys.normal_mode["<C-[>"] = ":bprev<cr>" -- go to left tab
lvim.keys.normal_mode["<C-]>"] = ":bnext<cr>" -- go to right tab
lvim.keys.insert_mode["jj"] = "<Esc>" -- I don't like reaching
lvim.builtin.treesitter.ensure_installed = {"python", "cpp", "c", "bash"} -- languages to check
lvim.builtin.treesitter.highlight.enable = true -- syntax check (I think)
vim.opt.clipboard = "unnamedplus" -- system clipboard access, if you have issues: sudo apt install xsel
